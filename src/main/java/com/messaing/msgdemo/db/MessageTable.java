package com.messaing.msgdemo.db;

import com.messaing.msgdemo.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageTable extends JpaRepository<Message, Long>{


}
