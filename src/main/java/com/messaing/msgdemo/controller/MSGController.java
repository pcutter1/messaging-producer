package com.messaing.msgdemo.controller;

import com.messaing.msgdemo.model.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.messaing.msgdemo.service.MSGService;

@RestController
@RequestMapping("/messages")
public class MSGController {

  MSGService msgService;

  public MSGController(MSGService msgService) {
    this.msgService = msgService;
  }

  @PostMapping
  public ResponseEntity<?> postMessages (@RequestBody Message message) {
    msgService.send(message);
    return ResponseEntity.accepted().build();
  }


}
