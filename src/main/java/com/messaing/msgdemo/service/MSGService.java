package com.messaing.msgdemo.service;

import com.messaing.msgdemo.model.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MSGService {

  @Value("${spring.rabbitmq.exchange}")
  private String exchange;
  @Value("${spring.rabbitmq.routingkey}")
  private String routingKey;

private RabbitTemplate rabbitTemplate;

  public MSGService(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }

  public void send(Message message) {
    rabbitTemplate.convertAndSend(exchange, routingKey, message);
  }

}
